<?php

// to do theme mail for node_blaster
$description = 'regular';
$context = 'page';

/** 
 * theme_epublish_layout_regular: A themeable function to produce a list of section topics and their nodes.
 *
 * @param $topics
 *   An array of topics
 * @param $params
 *   An associative array of parameters. This exists so that the function
 *   can be themed to handle whatever value(s) the theme designer wants passed
 *   into the layout. For example, an image might be passed into the function
 *   using $params = ('image_path' => $url, 'image_width' => $width, 'image_height' => $height, 'caption' => $caption)
 *
 * @return
 *   An HTML-formatted list of topics and their nodes.
 */
function theme_node_blaster_layout_regular($topics, $params=NULL) {
  // TODO 
  return '';
}
?>